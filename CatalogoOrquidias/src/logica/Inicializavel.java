package logica;

import java.awt.Container;

public interface Inicializavel {

	public void inicializar(Container container);

}

package logica;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * Classe que fica responsavel pelas buscas por nome, data propietario efim.
 * 
 * So altere essa classe se tiver 100% de certeza ou se tiver o backuo no BITBUCKER pq n�o to entendendo como isso ta funcionando
 */
public class Busca {

	private static ArrayList<Flor> todasFlores;

	@SuppressWarnings("resource")
	public static ArrayList<Flor> buscaFLores(){
		todasFlores = new ArrayList<>();
		File arqCodigos = new File(Caminhos.addCodigo);

		Scanner lerCodigos = null;
		try {

			lerCodigos = new Scanner(arqCodigos);

		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null," ERRO AO BUSCAR ");
		}
		try{
			while(lerCodigos.hasNext()){
				String codigo = lerCodigos.nextLine();
				if(codigo == ""){
					return todasFlores;
				}
				String caminhoADD = Caminhos.salvarFlor + codigo + ".lfe";
				Flor florLida = Flor.lerFlorArquivo(caminhoADD);
				todasFlores.add(florLida);
			}
		}catch(Exception e){JOptionPane.showMessageDialog(null," ERRO AO BUSCAR\nAdicionar array ");
		}
		return todasFlores;
	}

	public int contaVivas(){
		int contador = 0;
		todasFlores = buscaFLores(); 
		
		for (Flor flor : todasFlores) 
		{
			if(! flor.getSituacao()) //Se tiver morta ira incrementar 1 no contador
			{
				contador++;
			}
		}
		return contador; //COmo contamos as mortas para saber as vivas tiramos o total menoas as mortas.
	}

	public int contaMortas(){
		int vivas = contaVivas();
		return todasFlores.size() - vivas;
	}

	public int contaData(String data){
		int contador = 0;

		for (Flor flor : todasFlores) 
		{
			if(flor.getchegada() == data) //Se a data tiver exatamente igual conta
			{
				contador++;
			}
		}
		return contador ;
	}

	public ArrayList<Flor> buscaNome(String nomeFlor){
		ArrayList<Flor> FlorNome = new ArrayList<>();

		ArrayList<String> resultados = new ArrayList<>();
		ArrayList<Flor> pesquisa = Busca.buscaFLores();
		for (Flor flor : pesquisa){
			String nomeFlorArquivo = flor.getName();
			if (nomeFlor.equals(nomeFlorArquivo)) {
				resultados.add(flor.getCodigo());
			}
		}

		for (String codigo : resultados) {
			String path = Caminhos.salvarFlor + codigo + ".lfe";
			Flor florPesquisa = Flor.lerFlorArquivo(path);
			FlorNome.add(florPesquisa);
		}


		return FlorNome;
	}

	public ArrayList<Flor> buscaPropietario(String propietarioFlor){
		ArrayList<Flor> pesquisaPropietario = new ArrayList<>();

		ArrayList<String> resultados = new ArrayList<>();
		ArrayList<Flor> pesquisa = Busca.buscaFLores();
		for (Flor flor : pesquisa) {
			if (propietarioFlor.equals(flor.getPropietario())) {
				resultados.add(flor.getCodigo());
			}
		}

		for (String codigo : resultados) {
			String path = Caminhos.salvarFlor + codigo + ".lfe";
			Flor florPesquisa = Flor.lerFlorArquivo(path);
			pesquisaPropietario.add(florPesquisa);
		}
		return pesquisaPropietario;
	}


	public ArrayList<Flor> buscaData(String pesquisaData) {
		ArrayList<Flor> datas = new ArrayList<>();

		ArrayList<String> resultados = new ArrayList<>();
		ArrayList<Flor> pesquisa = Busca.buscaFLores();
		for (Flor flor : pesquisa) 
		{
			if(pesquisaData.equals(flor.getchegada())){
				resultados.add(flor.getCodigo());
					}
		}

		for (String codigo : resultados) {
			String path = Caminhos.salvarFlor + codigo + ".lfe";
			Flor florPesquisa = Flor.lerFlorArquivo(path);
			datas.add(florPesquisa);
		}
		return datas;
	}

	public ArrayList<Flor> buscaVivas(){
		ArrayList<Flor> floresBuscadas = new ArrayList<>();
		todasFlores = buscaFLores();	
		for (Flor flor : todasFlores) {
			if(! flor.getSituacao()){
				floresBuscadas.add(flor);
				flor.toString();
			}
		}
		return floresBuscadas;
	}
	
	public ArrayList<Flor> buscaMortas(){
		ArrayList<Flor> floresBuscadas = new ArrayList<>();
		todasFlores = buscaFLores();	
		for (Flor flor : todasFlores) {
			if(flor.getSituacao()){
				floresBuscadas.add(flor);
				flor.toString();
			}
		}
		return floresBuscadas;
	}

}

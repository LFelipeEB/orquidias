package logica;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class ImprimiPDF {

	public ImprimiPDF(){

	}

	public void escreveTodas(){
		Document documento = new Document();

		try{
			String path = Caminhos.imprimePdf+"ListaTodas.pdf";
			PdfWriter.getInstance(documento, new FileOutputStream(path));
			documento.open();

			PdfPTable tabela = new PdfPTable(new float[]{0.1f,0.175f,0.175f,0.125f,0.1f,0.175f,0.15f});
			tabela.setWidthPercentage(95.0f);
			tabela.setHorizontalAlignment(Element.ALIGN_CENTER);
			Paragraph titulo = new Paragraph("Lista de todas Orquidias cadastradas no Orquidario ... ");
			titulo.setAlignment(Element.ALIGN_CENTER);
			PdfPCell cabecalho = new PdfPCell(titulo);
			cabecalho.setColspan(7);
			tabela.addCell(cabecalho);
			tabela.addCell("Codigo");
			tabela.addCell("Nome");
			tabela.addCell("Cor");
			tabela.addCell("Data");
			tabela.addCell("Situ��o");
			tabela.addCell("Propietario");
			tabela.addCell("Numero de Floradas");

			ArrayList<Flor> arrayFlor = Busca.buscaFLores();
			System.out.println(arrayFlor.get(0).toString());
			if(arrayFlor.size() > 0){
				for(Flor flor : arrayFlor)
				{
					tabela.addCell(flor.getCodigo());
					tabela.addCell(flor.getName());
					tabela.addCell(flor.getCores());
					tabela.addCell(flor.getchegada());
					String situcao = flor.getSituacao() ? "MORTA" : "VIVA";
					tabela.addCell(situcao);
					tabela.addCell(flor.getPropietario());
					tabela.addCell(Integer.toString(flor.getFloracao().size()));
				}
			}else{
				JOptionPane.showMessageDialog(null, "NAO TEM NENHUMA FLOR CADASTRADA COM ESTAS CARACTERISTICAS");
			}

			documento.add(tabela);

		}catch(Exception e){ System.err.println("ERRO");}

		documento.close();
		mensagemConcluido("ListaTodas.pdf");

	}

	public void escreveVivas(){

		Document documento = new Document();

			String path = Caminhos.imprimePdf+"ListaVivas.pdf";
			try {
				PdfWriter.getInstance(documento, new FileOutputStream(path));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			documento.open();

			PdfPTable tabela = new PdfPTable(new float[]{0.1f,0.175f,0.175f,0.125f,0.1f,0.175f,0.15f});
			tabela.setWidthPercentage(95.0f);
			tabela.setHorizontalAlignment(Element.ALIGN_CENTER);
			Paragraph titulo = new Paragraph("Lista de Orquidias Vivas no Orquidario ... ");
			titulo.setAlignment(Element.ALIGN_CENTER);
			PdfPCell cabecalho = new PdfPCell(titulo);
			cabecalho.setColspan(7);
			tabela.addCell(cabecalho);
			tabela.addCell("Codigo");
			tabela.addCell("Nome");
			tabela.addCell("Cor");
			tabela.addCell("Data");
			tabela.addCell("Situ��o");
			tabela.addCell("Propietario");
			tabela.addCell("Numero de Floradas");

			Busca buscador = new Busca();

			ArrayList<Flor> arrayFlor = buscador.buscaVivas();
			if(arrayFlor.size() >0){
				for(Flor flor : arrayFlor)
				{
					tabela.addCell(flor.getCodigo());
					tabela.addCell(flor.getName());
					tabela.addCell(flor.getCores());
					tabela.addCell(flor.getchegada());
					String situcao = flor.getSituacao() ? "MORTA" : "VIVA";
					tabela.addCell(situcao);
					tabela.addCell(flor.getPropietario());
					tabela.addCell(Integer.toString(flor.getFloracao().size()));
					System.out.println(flor.toString());
				}
			}
			else{
				JOptionPane.showMessageDialog(null, "NAO TEM NENHUMA FLOR CADASTRADA COM ESTAS CARACTERISTICAS");
			}
			try {
				documento.add(tabela);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		
		
		documento.close();

		mensagemConcluido("ListaVivas.pdf");

	}

	public void escreveMortas(){
		Document documento = new Document();

		try{
			String path = Caminhos.imprimePdf+"ListaMortas.pdf";
			PdfWriter.getInstance(documento, new FileOutputStream(path));
			documento.open();

			PdfPTable tabela = new PdfPTable(new float[]{0.1f,0.175f,0.175f,0.125f,0.1f,0.175f,0.15f});
			tabela.setWidthPercentage(95.0f);
			tabela.setHorizontalAlignment(Element.ALIGN_CENTER);
			Paragraph titulo = new Paragraph("Lista de todas Orquidias cadastradas no Orquidario ... ");
			titulo.setAlignment(Element.ALIGN_CENTER);
			PdfPCell cabecalho = new PdfPCell(titulo);
			cabecalho.setColspan(7);
			tabela.addCell(cabecalho);
			tabela.addCell("Codigo");
			tabela.addCell("Nome");
			tabela.addCell("Cor");
			tabela.addCell("Data");
			tabela.addCell("Situ��o");
			tabela.addCell("Propietario");
			tabela.addCell("Numero de Floradas");

			Busca buscador = new Busca();

			ArrayList<Flor> arrayFlor = buscador.buscaMortas();
			System.out.println(arrayFlor.get(0).toString());
			if(arrayFlor.size() > 0){
				for(Flor flor : arrayFlor)
				{
					tabela.addCell(flor.getCodigo());
					tabela.addCell(flor.getName());
					tabela.addCell(flor.getCores());
					tabela.addCell(flor.getchegada());
					String situcao = flor.getSituacao() ? "MORTA" : "VIVA";
					tabela.addCell(situcao);
					tabela.addCell(flor.getPropietario());
					tabela.addCell(Integer.toString(flor.getFloracao().size()));
				}
			}	
			else{
				JOptionPane.showMessageDialog(null, "NAO TEM NENHUMA FLOR CADASTRADA COM ESTAS CARACTERISTICAS");
			}


			documento.add(tabela);

			mensagemConcluido("ListaMortas.pdf");

		}catch(Exception e){ System.err.println("ERRO");}

		documento.close();

	}

	/**
	 * Imprimido com sucesso, voc� conferir a impress�o em C:/Orquidias/PDFs/ "msg"
	 * @param msg , deve ser passado o nome do arquivo que foi impresso
	 */
	private void mensagemConcluido(String msg){
		JOptionPane.showMessageDialog(null,"Imprimido com sucesso, voc� conferir a impress�o em "
				+ "\nC:/Orquidias/PDFs/" +msg );
	}
}

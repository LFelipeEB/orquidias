package logica;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * Eu acrescentei so uma compara��o ternaria
 * 
 * que retorna de acordo com a avalia��o da conficao
 * 
 * (condicao) ? verdadeiro : falso;
 */

public class Flor implements Serializable {

	private static final long serialVersionUID = 1L;
	private String nomeCientifico, propietario, codigo, cor;
	private String chegada;
	private ArrayList<String> floracao;
	private boolean situacaoVida;
	private ImageIcon imagem;
	private static ObjectInputStream objectInputStream;
	
	
	public Flor(String nome, String propietario, String codigo, String chegada,
			ArrayList<String> floracao, String cor, boolean morta, ImageIcon imagem) {
		this.nomeCientifico = nome;
		this.propietario = propietario;
		this.chegada = chegada;
		this.codigo = codigo;
		this.floracao = floracao;
		this.cor = cor;
		this.situacaoVida = morta;
		this.imagem = imagem;
	}

	public Flor getFlor() {
		return this;
	}

	public void setFlor(String nome, String propietario, String codigo,
			String chegada, ArrayList<String> floracao) {
		this.nomeCientifico = nome;
		this.propietario = propietario;
		this.chegada = chegada;
		this.codigo = codigo;
		this.floracao = floracao;
	}

	public void mostraFLor() {

		String msg = "Flor Adicionada com sucesso:\n"+this.toString();
		JOptionPane.showMessageDialog(null, msg);
	}

	public void salvaFlor(String path) {
		File file = new File(path);
		file.delete();
		try{
		FileOutputStream arquivo = new FileOutputStream(path);
		ObjectOutputStream objetoGravado = new ObjectOutputStream(arquivo);
		objetoGravado.writeObject(this);
		objetoGravado.flush();
		objetoGravado.close();
		arquivo.flush();
		arquivo.close();
		}catch(Exception e){System.err.println(e.toString());}

		mostraFLor();
	}

	public static Flor lerFlorArquivo(String path) {
		Flor retorno = null;
		try {
			FileInputStream arquivoLeitura = new FileInputStream(path);
			objectInputStream = new ObjectInputStream(arquivoLeitura);
			retorno = (Flor) objectInputStream.readObject();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao ler do arquivo\nMetodo lerFlor ");
		}

		return retorno;
	}

	public String getName() {
		return nomeCientifico;
	}

	public String getPropietario() {
		return propietario;
	}

	public String getchegada() {
		return chegada;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getCores() {
		return cor;
	}

	public ArrayList<String> getFloracao() {
		return floracao;
	}
	
	/**
	 * 
	 * @return TRUE para as flores mortas e FALSE para as vivas
	 */
	public boolean getSituacao(){
		return situacaoVida;
	}
	
	public ImageIcon getImage(){
		return imagem;
	}

	@Override
	public String toString() {

		return ("Nome: " + nomeCientifico + "\nCodigo: " + codigo
				+ "\nPropietario: " + propietario + "\nCor: " + cor
				+ "\nData de Chegada: " + chegada + "\nSitua��o: " + (this.situacaoVida == true ? "Morta\n"
					: "Viva\n"+floracao.toString()));

	}
}

package logica;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JOptionPane;

/**
 * Interface apenas para guarda os endere�os dos arquivos ultilizado darante todo o codigo
 * Acredito que um enum seria melhor porem como n�o lembro a defini��o de enum criei uma inteface.
 * @author evari_000
 *
 */
public interface Caminhos {

	String salvarFlor = "C:/Orquidias/Arquivos/";
	String addCodigo = "C:/Orquidias/Pesquisas/codigos.txt";
	String iconeImage = "/imagens/orquidiaIcon.png";
	String imprimePdf = "C:/Orquidias/PDFs/";

	public class Diretorios{

		public Diretorios() {
			Path path = Paths.get(Caminhos.salvarFlor).getParent();
			if(Files.exists(path))
			{
				System.out.println("EXISTE PARENT");
			}
			else{
				System.out.println("CRIOU PARENT");
				try {
					Files.createDirectories(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			path = Paths.get(Caminhos.salvarFlor);
			if(Files.exists(path))
			{
				System.out.println("EXISTE ARQUIVOS");
			}
			else{
				System.out.println("CRIOU Arquivs");
				try {
					Files.createDirectories(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			path = Paths.get(Caminhos.addCodigo).getParent();
			if(Files.exists(path))
			{
			}
			else{
				try {
					Files.createDirectories(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			path = Paths.get(Caminhos.imprimePdf);
			if(Files.exists(path))
			{
			}
			else{
				try {
					Files.createDirectories(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			
			if(verificaCaminhos()){
				System.exit(0);
			}
			
		}
		
		private boolean verificaCaminhos(){
			Path path = Paths.get(Caminhos.salvarFlor);
			if(! Files.exists(path)){
				JOptionPane.showMessageDialog(null, "DIRETORIO COM INFORMA��ES SOBRE SOBRE FLORES NAO ENCONTRADO");
				return true;
			}
			path = Paths.get("C:/Orquidias/Pesquisas/");
			if(! Files.exists(path)){
				JOptionPane.showMessageDialog(null, "DIRETORIO COM INFORMA��ES SOBRE SOBRE PESQUISAS N�O ENCONTRADO");
				return true;
			}
			path = Paths.get(Caminhos.imprimePdf);
			if(! Files.exists(path)){
				JOptionPane.showMessageDialog(null, "DIRETORIO COM INFORMA��ES SOBRE SOBRE PFDs N�O ENCONTRADO");
				return true;
			}
						
			return false;
		}
	}
}

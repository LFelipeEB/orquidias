package logica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFormattedTextField;
import javax.swing.JTextField;

/**
 * Responsavel por validar os campos digitados pelo usuario, fazendo
 * retornar algum erro e nao permitir o preenchimento do registro com dados
 * errados ou nao padronizados
 * 
 * @author Luciano
 *
 */
public class Validador {

	private BufferedReader br;

	public boolean validaNome(JTextField campoNomeCientifico) {
		String nome = campoNomeCientifico.getText();
		return nome != null && !(nome.isEmpty()) ;
	}

	public boolean validaProprietario(JTextField campoProprietaria) {
		String proprietario = campoProprietaria.getText(); 
		return proprietario != null && !(proprietario.isEmpty());
	}

	public boolean validaCor(JTextField campoCor) {
		String cor = campoCor.getText();
		return cor != null && !(cor.isEmpty());
	}

	public boolean validaData(JFormattedTextField jftData, String dataChegada) {

		String data = jftData.getText();

		Calendar calendario = Calendar.getInstance();

		calendario.set(2000, 0, 1);

		if(data != null && !(data.equals("__/__/__"))) {

			String[] datas = data.split("/");

			int dia = Integer.parseInt(datas[0]);
			int mes = Integer.parseInt(datas[1]) - 1;
			int ano = Integer.parseInt(datas[2]);

			calendario.roll(Calendar.DAY_OF_MONTH, dia);
			calendario.roll(Calendar.MONTH, mes);
			calendario.roll(Calendar.YEAR, ano);

			Date date = calendario.getTime();

			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			dataChegada = format.format(date);
			return true; 
		}

		return false;
	}

	public boolean validaData(JFormattedTextField jftData) {

		String data = jftData.getText();

		if(data != null && !(data.equals("__/__/__"))) {
			return true; 
		}

		return false;
	}
	
	// varificar se ja existe o codigo
	public boolean validaCodigo(JFormattedTextField jftCodigo) {
		String cod = jftCodigo.getText();

		try {
			File f = new File(Caminhos.addCodigo);

			do {

				if(f.exists()) {
					br = new BufferedReader(new InputStreamReader(new FileInputStream(Caminhos.addCodigo)));
					String codAtual = null;
					if(f.length() > 1) {
						while((codAtual = br.readLine()) != null) {

							if(codAtual.equals(cod)) {
								return false;
							}
						}
					} 					
				} else {
					f.createNewFile();
				}
			} while(!f.exists());

		} catch (IOException e) {
			// arquivo nao encontrado ��
			e.printStackTrace();
		}

		return cod != null && !(cod.equals("____-_"));
	}
}

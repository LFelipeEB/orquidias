package GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.MaskFormatter;

import logica.Flor;
import logica.Validador;

import org.eclipse.wb.swing.FocusTraversalOnArray;

public class Campos extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField campoNome, campoCor, campoPropietaria;
	private JFormattedTextField jfRegistro, jfData;
	private JCheckBox chckbxMorta;
	private JButton btnLimparCampos;
	private JPanel labels, campos, chequeBotao, icones;
	private JLabel erroNome, erroRegistro, erroCor, erroData, erroPropietaria;
	private ImageIcon iconeVermelho, iconeAzul;
	/**
	 * Create the panel.
	 */
	public Campos() {
		setLayout(new BorderLayout(0, 0));

		iconeVermelho = new ImageIcon(Campos.class.getResource("/imagens/ic_error_red.png"));
		iconeAzul = new ImageIcon(Campos.class.getResource("/imagens/ic_info_blue.png"));

		labels = new JPanel();
		add(labels, BorderLayout.WEST);
		labels.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel lblNome = new JLabel("Nome:");
		labels.add(lblNome);

		JLabel lblRegistro = new JLabel("Regristro:");
		labels.add(lblRegistro);

		JLabel lblCor = new JLabel("Cor:");
		labels.add(lblCor);

		JLabel lblData =new JLabel("Data:");
		labels.add(lblData);

		JLabel lblPropietaria = new JLabel("Propietaria");
		labels.add(lblPropietaria);

		campos = new JPanel();
		add(campos, BorderLayout.CENTER);
		campos.setLayout(new GridLayout(0, 1, 0, 0));

		campoNome = new JTextField();
		campos.add(campoNome);
		campoNome.setHorizontalAlignment(SwingConstants.CENTER);

		MaskFormatter mascaraCodigo = null;
		try{
			mascaraCodigo = new MaskFormatter("####-U");
			mascaraCodigo.setPlaceholderCharacter('_');
		}catch(Exception e){}
		jfRegistro = new JFormattedTextField(mascaraCodigo);
		campos.add(jfRegistro);
		jfRegistro.setHorizontalAlignment(SwingConstants.CENTER);

		campoCor = new JTextField();
		campos.add(campoCor);
		campoCor.setHorizontalAlignment(SwingConstants.CENTER);

		MaskFormatter mascaraData = null;
		try{
			mascaraData = new MaskFormatter("##/##/##");
			mascaraData.setPlaceholderCharacter('_');
		}catch(Exception e){}
		jfData = new JFormattedTextField(mascaraData);
		campos.add(jfData);
		jfData.setHorizontalAlignment(SwingConstants.CENTER);

		campoPropietaria = new JTextField();
		campos.add(campoPropietaria);
		campoPropietaria.setHorizontalAlignment(SwingConstants.CENTER);
		campos.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{campoNome, jfRegistro, campoCor, jfData, campoPropietaria}));

		chequeBotao = new JPanel();
		add(chequeBotao, BorderLayout.SOUTH);
		chequeBotao.setLayout(new GridLayout(0, 2, 0, 0));

		chckbxMorta = new JCheckBox("Morta");
		chequeBotao.add(chckbxMorta);
		chckbxMorta.setHorizontalAlignment(SwingConstants.CENTER);

		btnLimparCampos = new JButton("Limpar Campos");
		chequeBotao.add(btnLimparCampos);

		icones = new JPanel();
		add(icones, BorderLayout.EAST);
		icones.setLayout(new GridLayout(0, 1, 0, 0));

		erroNome = new JLabel("");
		erroNome.setIcon(iconeVermelho);
		icones.add(erroNome);

		erroRegistro = new JLabel("");
		erroRegistro.setIcon(iconeVermelho);
		icones.add(erroRegistro);

		erroCor = new JLabel("");
		erroCor.setIcon(iconeVermelho);
		icones.add(erroCor);

		erroData = new JLabel("");
		erroData.setIcon(iconeVermelho);
		icones.add(erroData);

		erroPropietaria = new JLabel("");
		erroPropietaria.setIcon(iconeVermelho);
		icones.add(erroPropietaria);
		limpaCampos();
		trocaIcones().start();
	}

	private void limpaCampos(){
		btnLimparCampos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				campoNome.setText("");
				jfRegistro.setText("");
				campoCor.setText("");
				jfData.setText("");
				campoPropietaria.setText("");
				chckbxMorta.setSelected(false);
			}
		});
	}

	public void limpaTela(){
		campoNome.setText("");
		jfRegistro.setText("");
		campoCor.setText("");
		jfData.setText("");
		campoPropietaria.setText("");
		chckbxMorta.setSelected(false);

	}
	private Thread trocaIcones(){
		Validador validador = new Validador();
		return new Thread(new Runnable() {
			@Override
			public void run() {
				for(;;){
					ImageIcon temp = (validador.validaNome(campoNome)) ? null : iconeVermelho;
					erroNome.setIcon(temp);

					temp = (validador.validaCodigo(jfRegistro)) ? iconeAzul : iconeVermelho;
					erroRegistro.setIcon(temp);

					temp = (validador.validaCor(campoCor)) ? null : iconeVermelho;
					//ESTA DANDO ERRO
					temp = (validador.validaData(jfData)) ? iconeAzul : iconeVermelho;
					//ERRO NA DATA
					temp = (validador.validaProprietario(campoPropietaria)) ? null : iconeVermelho;
				}
			}
		} , "verificaIcones");
	}

	public String getCampoNome(){
		return campoNome.getText().toString();
	}
	public String getCampoCodigo(){
		return jfRegistro.getText().toString();
	}
	public String getCampoCor(){
		return campoCor.getText().toString();
	}

	public String getCampoData(){
		return jfData.getText().toString();
	}

	public String getCampoDono(){
		return campoPropietaria.getText().toString();
	}

	public boolean getSitucao(){
		return chckbxMorta.isSelected();
	}

	public void setCampos(Flor flor) {
		campoNome.setText(flor.getName());
		campoCor.setText(flor.getCores());
		campoPropietaria.setText(flor.getPropietario());
		jfRegistro.setText(flor.getCodigo());
		jfData.setText(flor.getchegada());
		chckbxMorta.setSelected(flor.getSituacao());
	}

	public void setCodigoEditable(boolean arg0){
		jfRegistro.setEditable(arg0);
	}
	public void setEnable(boolean agr0){
		btnLimparCampos.setEnabled(agr0);
		campoNome.setEditable(agr0);
		campoCor.setEditable(agr0);
		campoPropietaria.setEditable(agr0);
		jfData.setEditable(agr0);
		jfRegistro.setEditable(agr0);
		chckbxMorta.setEnabled(agr0);
	}
	
	public Flor getFlor(String codigo, ArrayList<String> floracao, ImageIcon imagem){
		String nome = getCampoNome();
		String cor = getCampoCor();
		String data = getCampoData();
		String proprietario = getCampoDono();
		boolean situacao = getSitucao();
		
		Flor flor = new Flor(nome, proprietario, codigo, data, floracao, cor, situacao, imagem);
		return flor;
	}
}
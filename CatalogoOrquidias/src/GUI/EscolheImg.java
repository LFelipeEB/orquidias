package GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import logica.Caminhos;

public class EscolheImg extends JPanel {

	private static final long serialVersionUID = 1L;
	private JButton btnEscolheImg;
	private JLabel lblImagem;
	/**
	 * Create the panel.
	 */
	public EscolheImg() {
		setLayout(new BorderLayout(0, 0));
		
		lblImagem = new JLabel("");
		lblImagem.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblImagem, BorderLayout.CENTER);
		lblImagem.setSize(getWidth(), 300);
		ImageIcon temp = new ImageIcon(EscolheImg.class.getResource(Caminhos.iconeImage));
		temp.setImage(temp.getImage().getScaledInstance(100, 100, 100));
		lblImagem.setIcon(temp);
		
		btnEscolheImg = new JButton("Escolher Imagem");
		add(btnEscolheImg, BorderLayout.SOUTH);
		escolheImg();
		btnEscolheImg.setSize(300, getHeight());
	}

	private void escolheImg(){
		btnEscolheImg.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fcImg = new JFileChooser();
				fcImg.showOpenDialog(null);
				fcImg.setFileSelectionMode(JFileChooser.FILES_ONLY);
				fcImg.setDialogTitle("Selecione uma Imagem");
				ImageIcon retorno = new ImageIcon(fcImg.getSelectedFile().getPath());
				retorno.setImage(retorno.getImage().getScaledInstance(400,225,100));
				lblImagem.setIcon(retorno);
			}
		});
	}
	
	public ImageIcon getImage(){
		return (ImageIcon) lblImagem.getIcon();
	}

	public void setEnable(boolean arg0){
		btnEscolheImg.setEnabled(arg0);
	}
	
	public void setImagem(ImageIcon image) {
		lblImagem.setIcon(image);
	}
	
	public void limpaImg(){
		lblImagem.setIcon(new ImageIcon(EscolheImg.class.getResource(Caminhos.iconeImage)));
	}
}

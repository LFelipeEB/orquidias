package GUI;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.text.MaskFormatter;

import logica.Caminhos;
import logica.Flor;
import logica.Inicializavel;

public class Consultar implements Inicializavel {
	private JButton btnPesquisar;
	private JFormattedTextField tfNome;
	private Tabela tabela;
	private Campos campos;
	private EscolheImg img;
	private MaskFormatter mascaraPesquisa;
	private static int tipoPesquisa = 2 ;
	private JButton btnAtualizarDados, btnSalvar;
	private int widthPrincipal;
	private int heightPrincipal;
	private Container containerPrincipal;

	public static int PesquisaNome=1;
	public static int PesquisaCodigo= 2;
	public static int PesquisaPropietario=3;

	private static Consultar janelaConsultar;

	/**
	 * @wbp.parser.entryPoint
	 */
	private Consultar() {

	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void init() {
		initialize();

		/* ------- BARRA DE MENU --------- */

		// //////////////////////////////////////////
		//
		// /*------------ PESQUISAR ------------ */
		//
		// /////////////////////////////////////////
		btnPesquisar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				campos.limpaTela();
				tabela.limpaTabela();
				img.limpaImg();

				// PESQUISA POR CODIGO
				if (Consultar.this.tipoPesquisa == 2) {
					String pesquisa = tfNome.getText().toString();
					pesquisa = Caminhos.salvarFlor + pesquisa + ".lfe";
					File temp = new File(pesquisa);
					if (temp.isFile()) {
						Flor florPesquisa = Flor.lerFlorArquivo(pesquisa);
						campos.setCampos(florPesquisa);
						tabela.addDadosTabela(florPesquisa.getFloracao());
						img.setImagem(florPesquisa.getImage());
					} else {
						JOptionPane.showMessageDialog(null,
								"Pesquisa Invalida\nArquivo n�o encontrado");
					}
				}
			}
		});

		// //////////////////////////////////////////
		//
		// /*------------ Atualizar dados ------------ */
		//
		// /////////////////////////////////////////
		btnAtualizarDados.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				campos.setEnable(true);
				tabela.setEnable(true);
				img.setEnable(true);
				btnSalvar.setEnabled(true);
				campos.setCodigoEditable(false);
			}
		});

		// //////////////////////////////////////////
		//
		// /*------------ SALVAR ------------ */
		//
		// /////////////////////////////////////////
		btnSalvar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String codigo = campos.getCampoCodigo();
				String nome = campos.getCampoNome();
				String proprietario = campos.getCampoDono();
				String cor = campos.getCampoCor();
				String data = campos.getCampoData();
				boolean situacao = campos.getSitucao();
				ImageIcon imagem = img.getImage();
				ArrayList<String> flora��o = tabela.getDadosTabela();
				Flor flor = new Flor(nome, proprietario, codigo, data,
						flora��o, cor, situacao, imagem);
				String path = Caminhos.salvarFlor + codigo + ".lfe";
				flor.salvaFlor(path);

				campos.limpaTela();
				tabela.limpaTabela();

				tfNome.setText("");
				campos.setEnable(false);
				tabela.setEnable(false);
				img.setEnable(false);
				btnSalvar.setEnabled(false);
			}
		});
	}

	/**
	 * Esta fun��o inicializa a interface grafica da classe
	 * 
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		containerPrincipal.setLayout(new BoxLayout(containerPrincipal,
				BoxLayout.X_AXIS));

		JPanel panel = new JPanel();
		containerPrincipal.add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		panel.setSize(300, 300);

		if(tipoPesquisa == 2 ){//CODIGO 
			try {
				mascaraPesquisa = new MaskFormatter("####-U");
				tfNome = new JFormattedTextField(mascaraPesquisa);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		if(tipoPesquisa == 1 || tipoPesquisa == 3){//NOME  || Propietario
			tfNome = new JFormattedTextField();
		}

		tfNome.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(tfNome);
		tfNome.setColumns(10);

		btnPesquisar = new JButton("Pesquisar");
		panel.add(btnPesquisar);

		btnAtualizarDados = new JButton("Atualizar Dados");
		panel.add(btnAtualizarDados);

		btnSalvar = new JButton("Salvar");
		panel.add(btnSalvar);
		btnSalvar.setEnabled(false);

		campos = new Campos();
		containerPrincipal.add(campos);
		campos.setEnable(false);
		tabela = new Tabela();
		containerPrincipal.add(tabela);
		tabela.setEnable(false);
		img = new EscolheImg();
		containerPrincipal.add(img);
		img.setEnable(false);
	}

	// mudar a logica de negocio
	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public void inicializar(Container container) {

		containerPrincipal = container;
		container.setVisible(true);
		widthPrincipal = container.getWidth();
		heightPrincipal = container.getHeight();

		// faz a magia
		System.out.println("Tela: Consulta. " + "Width: " + widthPrincipal
				+ ", Height: " + heightPrincipal);

		init();
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public static Inicializavel getInstance() {
		if (janelaConsultar == null) {
			janelaConsultar = new Consultar();
		}

		return janelaConsultar;
	}
	/**
	 * Este metodo deve ser chamado antes do metodo de inicializar a tela de consulta
	 * para ele setar o modo da pesquisa caso contario ela entrar no padrao (CODIGO) e se for
	 * inicializada a tela em varios lugares ele ira pegar o ultimo tipo de pesquisa setado.
	 * @param modo
	 */
	public static void setModoPesquisa(int modo){
		tipoPesquisa = modo;
	}
}

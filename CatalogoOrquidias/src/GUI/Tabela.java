package GUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

public class Tabela extends JPanel {


	private static final long serialVersionUID = 1L;
	private JTable table;
	private JFormattedTextField ftfDatas;
	private ArrayList<String> dadosTabela, dadosTemp;
	private JButton btnInserirData, btnApagaDadosInseridos ;
	private DefaultTableModel modeloTabela;
	private int contadorLinhas;
	/**
	 * Ao ser construido este metodo desenha a tabela, construindo o JPanel.
	 * 
	 */
	public Tabela() {
		inicializa();
		setLayout(new BorderLayout(0, 0));

		/* ---- CABE�ARIO TABELA ---*/

		JPanel cabe�alho = new JPanel();
		add(cabe�alho, BorderLayout.NORTH);
		cabe�alho.setLayout(new BorderLayout(0, 0));

		JLabel lblTabela = new JLabel("Tabela");
		lblTabela.setHorizontalAlignment(SwingConstants.CENTER);
		cabe�alho.add(lblTabela, BorderLayout.NORTH);

		JPanel panel = new JPanel();
		cabe�alho.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(1, 0, 0, 0));

		/* ------ MASCARA DA DATA -------*/

		MaskFormatter maskData = null;
		try {
			maskData = new MaskFormatter("##/##/##");
			maskData.setPlaceholder("_");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		ftfDatas = new JFormattedTextField(maskData);
		ftfDatas.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(ftfDatas);

		btnInserirData = new JButton("Inserir Data");
		panel.add(btnInserirData);

		/* ------ DESENHANDO TABELA ------*/

		Object[] colunas = new String[] {"Datas de Flora��o"};
		Object[][] linhas = new String[][] {};

		modeloTabela = new DefaultTableModel(linhas, colunas);
		table = new JTable(modeloTabela);
		table.setEnabled(false);
		JScrollPane scroll = new JScrollPane(table);
		add(scroll, BorderLayout.CENTER);
		btnApagaDadosInseridos = new JButton("Apaga dados inseridos");
		add(btnApagaDadosInseridos, BorderLayout.SOUTH);
		addDadoTabela();
		limpaDadosTemporario();
	}


	public ArrayList<String> getDadosTabela(){
		dadosTabela.addAll(dadosTemp);
		dadosTemp.clear();
		return dadosTabela;
	}

	private void inicializa(){
		dadosTabela = new ArrayList<>();
		dadosTemp = new ArrayList<>();
		contadorLinhas = 0;
	}

	/*
	 * Adiciona 1 dado na tabela, ultilizando no botao adicionar data.
	 */
	private void addDadoTabela(){
		btnInserirData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String data = ftfDatas.getText().toString();
				if(data.equals("  /  /  ") ||data.equals("_ /  /  ")  ){
					JOptionPane.showMessageDialog(null, "DATA INVALIDO FAVOR INSERIR UMA NOVA.");
					return;
				}
				Object[] obData = new String[] {data};
				modeloTabela.addRow(obData);
				dadosTemp.add(data);
				contadorLinhas++;
				ftfDatas.setText("");
			}
		});


	}

	/*
	 * Printa todos os dados do array recebido por parametro na tabela.
	 */
	public void addDadosTabela(ArrayList<String> array){
		ArrayList<String> arrayTemp = new ArrayList<>();
		for(int i =0; i < array.size();++i){
			arrayTemp.add(array.get(i));
			Object[] temp = arrayTemp.toArray();
			modeloTabela.addRow(temp);
			dadosTabela.add(array.get(i));
			arrayTemp.clear();
			//Eu estava tentando adicionar um array de objetos porem ele do adicionava o primeiro,
			// pois acredito que ele recebia um array sendo cada indice uma coluna, sendo assim,
			// fiz essa gambiarra apara a todo momento um posi��o ser o primeiro e adicionar todos.
		}
	}

	/*
	 * Exclui todos os dados inseridos no momentos, aqueles dados salvos no array nao pode ser excluido.
	 */
	private void limpaDadosTemporario(){
		btnApagaDadosInseridos.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(contadorLinhas > 0){
					dadosTemp.clear();
					int contadorTemp = contadorLinhas;
					for(int i= dadosTabela.size(); i < (dadosTabela.size() + contadorTemp);  ++i){
						modeloTabela.removeRow(dadosTabela.size());
						contadorLinhas--;
					}
				}else{JOptionPane.showMessageDialog(null, "Nenhum dado novo foi inserido");}
			}
		});
	}

	public JPanel desenhaTabela(){
		return this;
	}

	/*
	 * Limpa todos os dados impressos na tabela, e limpa dos os dados dos arrays que podem ser recuperados da tabela.
	 */
	public void limpaTabela() {
		int numeroLinhas = dadosTabela.size() + dadosTemp.size();
		System.out.println(numeroLinhas);
		for(int i=0; i < numeroLinhas; ++i)
		{
			modeloTabela.removeRow(0);
		}
		dadosTabela.clear();
		dadosTemp.clear();
	}

	public void setEnable(boolean arg0){
		btnApagaDadosInseridos.setEnabled(arg0);
		btnInserirData.setEnabled(arg0);
		ftfDatas.setEditable(arg0);
	}
}

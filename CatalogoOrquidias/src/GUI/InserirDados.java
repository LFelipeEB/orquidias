package GUI;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import logica.Caminhos;
import logica.Flor;
import logica.Inicializavel;

public class InserirDados implements Inicializavel {

	private static InserirDados janelaInserirDados;
	private int widthPrincipal;
	private int heightPrincipal;

	private Container containerPrincipal;

	private InserirDados() {

	}

	private void init() {

		containerPrincipal.setLayout(new BoxLayout(containerPrincipal,
				BoxLayout.X_AXIS));

		Campos campos = new Campos();
		containerPrincipal.add(campos);
		EscolheImg img = new EscolheImg();
		containerPrincipal.add(img);
		Tabela tabela = new Tabela();
		containerPrincipal.add(tabela);

		JPanel panel = new JPanel();
		containerPrincipal.add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		// analisar essa parte FOI MODIFICADA PELO LUIZ
		/* ----------- SALVAR ------------- */

		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String codigo = campos.getCampoCodigo();
				if("____-_".equals(codigo)){
					JOptionPane.showMessageDialog(null, "CODIGO INVALIDO FAVOR INSERIR UM NOVO.");
					return;
				}
				String nome = campos.getCampoNome();
				String proprietario = campos.getCampoDono();
				String cor = campos.getCampoCor();
				String data = campos.getCampoData();
				boolean situacao = campos.getSitucao();
				ImageIcon imagem = img.getImage();
				ArrayList<String> flora��o = tabela.getDadosTabela();
				Flor flor = new Flor(nome, proprietario, codigo, data,
						flora��o, cor, situacao, imagem);
				String path = Caminhos.salvarFlor + codigo + ".lfe";
				File confere = new File(path);
				if (confere.isFile())// Verifica se o arquivo existe;
				{
					JOptionPane.showMessageDialog(null,
							"O arquivo ja existe tente outro codigo");
				} else {
					flor.salvaFlor(path);
					addCodigo(codigo);
				}
			}
		});
		panel.add(btnSalvar);

		/* ----------- FECHAR ------------- */
		JButton btnFechar = new JButton("Fechar");
		btnFechar.setEnabled(true);
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		panel.add(btnFechar);

	}

	private void addCodigo(String codigo) {
		try {
			File txtArq = new File(Caminhos.addCodigo);
			FileWriter txtWrite = new FileWriter(txtArq, true);
			BufferedWriter bfw = new BufferedWriter(txtWrite);
			bfw.write(codigo);
			bfw.newLine();
			bfw.flush();
			bfw.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"Erro ao escrever no arquivo de pesquisa "
							+ e.getStackTrace().toString());

		}
	}

	// mudar a logica de negocio
	@Override
	public void inicializar(Container container) {

		containerPrincipal = container;

		widthPrincipal = container.getWidth();
		heightPrincipal = container.getHeight();

		System.out.println("Tela: InserirDados. " + "Width: " + widthPrincipal
				+ ", Height: " + heightPrincipal);

		init();

	}

	// verificar isso com nova logica de negocios
	public static Inicializavel getInstance() {

		if (janelaInserirDados == null) {
			janelaInserirDados = new InserirDados();
		}

		return janelaInserirDados;
	}
}

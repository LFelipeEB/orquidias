package home;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import logica.Busca;
import logica.Caminhos;
import logica.ImprimiPDF;
import logica.Inicializavel;
import GUI.Consultar;
import GUI.InserirDados;

@SuppressWarnings("serial")
public class Entrada extends JFrame {

	private JPanel panelCentral;
	private JLabel lblHome;
	private JPanel panelSul;
	private boolean ativaTelaDuvida;
	private boolean ativaTelaHome;
	private JLabel lblDuvidas;
	private JPanel panelContato;
	protected boolean clicadoInserirNovo = true;
	protected boolean clicadoPesquisaFlor = true;
	private JMenuBar menuBar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {

					Entrada frame = new Entrada();

					frame.setVisible(true);
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					int width = frame.getWidth() - 20;
					int height = frame.getHeight() - 55;
					frame.setSize(width, height);
					frame.setLocationRelativeTo(null);

					URL url = this.getClass().getResource("/imagens/orquidiaIcon.png");
					Image iconeTitulo = Toolkit.getDefaultToolkit().getImage(url);

					frame.setIconImage(iconeTitulo);

				} catch (Exception e) {
					alertaErro(
							null,
							"Erro em abrir janela principal: \n"
									+ e.fillInStackTrace());
				}
			}
		});
	}

	public Entrada() {
		super("Sistema de Controle de Orquidias");

		new Caminhos.Diretorios();
		
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		// ================================ Arquivo
		// =======================================================================

		JMenu mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);

		JMenuItem mntmInserirNovo = new JMenuItem("Inserir novo registro");
		mntmInserirNovo.setIcon(new ImageIcon(Entrada.class
				.getResource("/imagens/ic_add.png")));
		mntmInserirNovo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				clicadoInserirNovo = !clicadoInserirNovo;
				atualizarTelaPrincipal(InserirDados.getInstance(),
						clicadoInserirNovo);
			}
		});
		mnArquivo.add(mntmInserirNovo);

		JMenuItem mntmAtualizarRegistro = new JMenuItem("Atualizar registro");
		mntmAtualizarRegistro.setIcon(new ImageIcon(Entrada.class
				.getResource("/imagens/ic_sync.png")));
		mntmAtualizarRegistro.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				clicadoPesquisaFlor = !clicadoPesquisaFlor;
				atualizarTelaPrincipal(Consultar.getInstance(), clicadoPesquisaFlor);
			}
		});
		mnArquivo.add(mntmAtualizarRegistro);

		JMenuItem mntmSairDoPrograma = new JMenuItem("Sair do programa");
		mntmSairDoPrograma.setIcon(new ImageIcon(Entrada.class
				.getResource("/imagens/ic_exit.png")));
		mntmSairDoPrograma.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Clicou em: " + arg0.getActionCommand());
				dispose();
			}
		});
		mnArquivo.add(mntmSairDoPrograma);

		// ================================ Pesquisa
		// ======================================================================

		JMenu mnPesquisar = new JMenu("Pesquisar");
		menuBar.add(mnPesquisar);

		JMenu mntmPesquisarFlor = new JMenu("Pesquisar Flor");
		mntmPesquisarFlor.setIcon(new ImageIcon(Entrada.class
				.getResource("/imagens/ic_search.png")));
		// ================================ Pesquisa por codigo
		JMenuItem mntmPesquisar = new JMenuItem("Pesquisar por Codigo: ");
		mntmPesquisar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clicadoPesquisaFlor = !clicadoPesquisaFlor;
				Consultar.setModoPesquisa(Consultar.PesquisaCodigo);
				atualizarTelaPrincipal(Consultar.getInstance(), clicadoPesquisaFlor);
			}
		});
		mntmPesquisarFlor.add(mntmPesquisar);
		mnPesquisar.add(mntmPesquisarFlor);
		
		mnPesquisar.add(mntmPesquisarFlor);


		JMenu mntmPesquisarLote = new JMenu("Pesquisar Lote");
		mnPesquisar.add(mntmPesquisarLote);
		JMenuItem mntmPesquisaLoteNome = new JMenuItem("Pesquisar por Nome");
		mntmPesquisaLoteNome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String pesquisaNome = JOptionPane.showInputDialog("Digite o nome a ser pesquisado");
				Busca buscador = new Busca();
				new Tabela(buscador.buscaNome(pesquisaNome)).show();
			}
		});
		mntmPesquisarLote.add(mntmPesquisaLoteNome);

		mntmPesquisaLoteNome = new JMenuItem("Pesquisar por Propietario");
		mntmPesquisaLoteNome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String pesquisaNome = JOptionPane.showInputDialog("Digite o nome a ser pesquisado");
				Busca buscador = new Busca();
				new Tabela(buscador.buscaPropietario(pesquisaNome)).show();
			}
		});
		mntmPesquisarLote.add(mntmPesquisaLoteNome);

		mntmPesquisaLoteNome = new JMenuItem("Pesquisar por Data");
		mntmPesquisaLoteNome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String pesquisaNome = JOptionPane.showInputDialog("Digite o nome a ser pesquisado");
				Busca buscador = new Busca();
				new Tabela(buscador.buscaData(pesquisaNome)).show();
			}
		});
		mntmPesquisarLote.add(mntmPesquisaLoteNome);

		JMenuItem mntmListarTodasAs = new JMenuItem("Listar todas as Flores");
		mntmListarTodasAs.setIcon(new ImageIcon(Entrada.class
				.getResource("/imagens/ic_format_list_numbered.png")));
		mntmListarTodasAs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Tabela.main(null);
			}
		});
		mnPesquisar.add(mntmListarTodasAs);
		
		// ================================ Contador
		// =========================================================================
		JMenu numerosMenu =  new JMenu("Orquidario em Numeros");
		numerosMenu.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(numerosMenu);
		JMenu situacao = new JMenu("Situcao");
		numerosMenu.add(situacao);
		JMenuItem viva = new JMenuItem("VIVA");
		viva.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Busca buscador = new Busca();
				int contadas = buscador.contaVivas();
				JOptionPane.showMessageDialog(null, "O numero de flores vivas �: "+contadas);
			}
		});
		situacao.add(viva);
		
		viva = new JMenuItem("MORTAS");
		viva.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Busca buscador = new Busca();
				int contadas = buscador.contaMortas();
				JOptionPane.showMessageDialog(null, "O numero de flores mortas �: "+contadas);
			}
		});
		situacao.add(viva);
		
		// ================================ IMPRIMIR
		// =========================================================================

		JMenu imprimiMenu =  new JMenu("Imprimir");
		imprimiMenu.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(imprimiMenu);
		JMenuItem imprimir = new JMenuItem("Todas as vivas");
		imprimiMenu.add(imprimir);
		imprimir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new ImprimiPDF().escreveVivas();;
			}
		});
		
		imprimir = new JMenuItem("Todas");
		imprimiMenu.add(imprimir);
		imprimir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new ImprimiPDF().escreveTodas();
			}
		});
		
		imprimir = new JMenuItem("Todas as mortas");
		imprimiMenu.add(imprimir);
		imprimir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new ImprimiPDF().escreveMortas();;
			}
		});

		
		// ================================ Ajuda
		// =========================================================================

		JMenu helpMenu = new JMenu("Ajuda");
		helpMenu.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(helpMenu);
		JMenuItem sobreItem = new JMenuItem("Sobre");
		sobreItem.setIcon(new ImageIcon(Entrada.class
				.getResource("/imagens/ic_info_black.png")));
		sobreItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String sobre = " Aplicativo para auxiliar no controle de estoque\n"
						+ "\n Desenvolvido por:\n"
						+ "          Luiz Felipe Evaristo\n"
						+ "          Luciano Silva\n\n"
						+ " Estudades de Sistemas de Informa��o na UFVJM - Campus Diamantina"
						+ "\n\n Vers�o do aplicativo: 1.0\n Ano: 2015";

				alerta(getParent(), "Sobre", sobre);

			}
		});
		helpMenu.add(sobreItem);

		JMenuItem mntmComoUsar = new JMenuItem("Como usar");
		mntmComoUsar.setIcon(new ImageIcon(Entrada.class
				.getResource("/imagens/ic_help.png")));
		mntmComoUsar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Desktop desktop = Desktop.getDesktop();
				File file = new File("src/Manual.pdf");
				try {
					desktop.open(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		helpMenu.add(mntmComoUsar);

		JMenuItem mntmContatoDesenvolvedor = new JMenuItem(
				"Contato desenvolvedor");
		mntmContatoDesenvolvedor.setIcon(new ImageIcon(Entrada.class
				.getResource("/imagens/ic_email.png")));
		mntmContatoDesenvolvedor.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String contatoDeveloper = "\n                    Luiz Felipe Evaristo\n"
						+ "  Telefone:\n      (33) 9131-6542 (TIM - WhatsApp)\n      (38) 9733-2701 (VIVO)\n"
						+ "  E-mail:   lfelipeeb@gmail.com\n\n\n\n"
						+ "                    Luciano Silva\n"
						+ "  Telefone:\n      (38) 9944-4287 (Vivo - WhatsApp)\n"
						+ "  E-mail:   luciano.tj2011@gmail.com\n\n";

				alerta(getParent(), "Contato com desenvolvedor",
						contatoDeveloper);

			}
		});
		helpMenu.add(mntmContatoDesenvolvedor);

		// ================================ Painel Central
		// =========================================================

		panelCentral = new JPanel();
		panelCentral.setLayout(new BorderLayout(0, 0));
		getContentPane().add(panelCentral, BorderLayout.CENTER);

		setTelaHome(true);

		// ================================ Painel Inferior (Sul)
		// =========================================================

		panelSul = new JPanel();
		getContentPane().add(panelSul, BorderLayout.SOUTH);
		panelSul.setLayout(new BorderLayout());

		setTelaDuvida(false);

	}

	private void setTelaDuvida(boolean ativa) {

		setAtivaTelaDuvida(ativa);

		if (lblDuvidas == null)
			lblDuvidas = new JLabel("Duvidas ?");
		lblDuvidas.setHorizontalAlignment(SwingConstants.CENTER);

		if (panelContato == null)
			panelContato = new JPanel();
		panelContato.setLayout(new GridLayout(1, 3));

		JLabel lblTelefone = new JLabel("Telefone: (33) 9131-6542 (TIM)");
		lblTelefone.setSize(100, 50);
		panelContato.add(lblTelefone);

		JLabel lblvivo = new JLabel("(38) 9733-2701 (VIVO)");
		lblvivo.setHorizontalAlignment(SwingConstants.CENTER);
		panelContato.add(lblvivo);

		JLabel lblEmailLfelipeebgmailcom = new JLabel(
				"E-mail: lfelipeeb@gmail.com");
		lblEmailLfelipeebgmailcom.setHorizontalAlignment(SwingConstants.RIGHT);
		panelContato.add(lblEmailLfelipeebgmailcom);

		if (ativa) {
			panelSul.add(lblDuvidas, BorderLayout.NORTH);
			panelSul.add(panelContato);
		} else
			panelSul.remove(panelContato);

	}

	private void setTelaHome(boolean ativa) {

		setAtivaTelaHome(ativa);

		if (lblHome == null) {
			lblHome = new JLabel("");
			lblHome.setIcon(new ImageIcon(Entrada.class
					.getResource("/imagens/fundo_home2.png")));
			lblHome.setHorizontalAlignment(SwingConstants.CENTER);
		}

		lblHome.setVisible(ativa);

		if (ativa) {
			panelCentral.removeAll();
			panelCentral.invalidate();
			panelCentral.repaint();
			panelCentral.add(lblHome, BorderLayout.CENTER);
		} else {
			panelCentral.removeAll();
			panelCentral.invalidate();
			panelCentral.repaint();
		}
	}

	private static void alertaErro(Component janela, String erro) {

		JOptionPane.showMessageDialog(janela, erro, "Erro",
				JOptionPane.ERROR_MESSAGE);
	}

	private static void alerta(Component janela, String titulo, String mensagem) {

		JOptionPane.showMessageDialog(janela, mensagem, titulo,
				JOptionPane.PLAIN_MESSAGE);
	}

	private void atualizarTelaPrincipal(Inicializavel inicializavel, boolean jaClicado) {
		if (jaClicado) {
			setTelaHome(true);
		} else {
			setTelaHome(false);
			inicializavel.inicializar(panelCentral);
		}

	}

	public boolean isAtivaTelaDuvida() {
		return ativaTelaDuvida;
	}

	private void setAtivaTelaDuvida(boolean ativaTelaDuvida) {
		this.ativaTelaDuvida = ativaTelaDuvida;
	}

	public boolean isAtivaTelaHome() {
		return ativaTelaHome;
	}

	private void setAtivaTelaHome(boolean ativaTelaHome) {
		this.ativaTelaHome = ativaTelaHome;
	}
	
	public JMenuBar getJMenuBar(){
		return menuBar;
	}
}

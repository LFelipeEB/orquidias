package home;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import logica.Busca;
import logica.Flor;

public class Tabela extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tabela frame = new Tabela();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Eu comecei a mexer na tabela
	 * mas so tava vendo como funcionava o objeto Table
	 * n�o � muito dificil
	 */
	public Tabela() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		Object[] colunas = new String[] {"Codigo","Nome", "Cor", "Data", "Situa��o","Propietario","Numero de Floradas"};
		
		Object[][] dados = new String[][] {};
		
		table = new JTable();
		
		//usa pra editar a tabela
		DefaultTableModel mod = new DefaultTableModel(dados, colunas);
		table.setModel(mod);
		
		// recupera objeto e faz altera��o
		// ou recupera itens da table 
		DefaultTableModel modelo = (DefaultTableModel) table.getModel();
		ArrayList<Flor> array = Busca.buscaFLores();
		for(Flor flor : array){
			int floracao = flor.getFloracao().size();

			String situacao = flor.getSituacao() ? "MORTA":"VIVA";
			
			Object[] linha = new String[]{flor.getCodigo(),flor.getName(),flor.getCores(),
					flor.getchegada(),situacao,flor.getPropietario(), Integer.toString(floracao)};
			modelo.addRow(linha);
		}
		contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
		
	}
	
	public Tabela(ArrayList<Flor> florPesquisa) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		Object[] colunas = new String[] {"Codigo","Nome", "Cor", "Data", "Situa��o","Propietario","Numero de Floradas"};
		
		Object[][] dados = new String[][] {};
		
		table = new JTable();
		
		//usa pra editar a tabela
		DefaultTableModel mod = new DefaultTableModel(dados, colunas);
		table.setModel(mod);
		
		// recupera objeto e faz altera��o
		// ou recupera itens da table 
		DefaultTableModel modelo = (DefaultTableModel) table.getModel();
		ArrayList<Flor> array = florPesquisa;
		for(Flor flor : array){
			int floracao = flor.getFloracao().size();
			String situacao = flor.getSituacao() ? "VIVA":"MORTA";
			
			Object[] linha = new String[]{flor.getCodigo(),flor.getName(),flor.getCores(),
					flor.getchegada(),situacao,flor.getPropietario(), Integer.toString(floracao)};
			modelo.addRow(linha);
		}
		contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
		
	}


}
